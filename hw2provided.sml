(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
fun all_except_option(s:string,lists:string list)=
    let fun mklist(from:string list,to:string list)=
	case from of
	    []=>NONE
	  |x::xs=>if same_string(x,s) then SOME (to@xs) else mklist(xs,to@[x])
    in
	mklist(lists,[])
    end

fun get_substitutions2(strs:(string list) list,s:string)=
    let fun mklist(from:(string list)list,answer:string list)=
	    case from of
		[]=>answer
	      |x::xs=>case all_except_option(s,x) of
			  NONE=>mklist(xs,answer)
			|SOME z=>mklist(xs,answer@z)
    in 
	mklist(strs,[])
    end
    
fun get_substitutions1(strs:(string list) list,s:string)=
    case strs of
	[]=>[]
      |x::xs=>case all_except_option(s,x) of
		  NONE=>get_substitutions1(xs,s)
		|SOME z=>z@get_substitutions1(xs,s)
			 
fun similar_names(subs:string list list,name:{first:string, last:string, middle:string})=
    let fun make_names(first:string list,last:string,middle:string,ans:{first:string,middle:string,last:string}list)=
	case first of
	    []=>ans
	  |x::xs=>make_names(xs,last,middle,ans@[{first=x,middle=middle,last=last}]);
    in
	case name of {first=first,last=last,middle=middle}=>make_names(get_substitutions2(subs,first),last,middle,[name])
    end
    
(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color(card:card)=
    case card of
	(Clubs,_)=>Black
      |(Spades,_)=>Black
      |_=>Red
fun card_value(card:card)=
    case card of
	(_,Num n)=>n
      |(_,Ace)=>11
      |_=>10
fun remove_card(cs:card list,c:card,e:exn)=
    let fun look(from:card list,to:card list)=
	    case from of
		[]=>raise e
	      |x::xs=>if x=c then to@xs else look(xs,to@[x])
    in
	look(cs,[])
    end
fun all_same_color(cards:card list)=
    case cards of
	[]=>true
      |x::[]=>true
      |x::(y::z)=>if card_color x=card_color y then all_same_color(y::z) else false
fun sum_cards(cards:card list)=
    let fun summer(cards:card list,sum:int)=
	    case cards of
		[]=>sum
	      |x::xs=>summer(xs,sum+card_value(x))
    in
	summer(cards,0)
    end
fun score(cards:card list,goal:int)=
    let val initscore=if sum_cards(cards)<goal then goal-sum_cards(cards) else 3*(sum_cards(cards)-goal)
    in
	case all_same_color(cards) of
	    true=>Int.div(initscore,2)
	  |_=>initscore
    end
fun officiate(cards:card list,moves:move list,goal:int)=
    let fun play(cards:card list,moves:move list,hand:card list)=
	    case goal<sum_cards(hand) of
		true=>hand
	      |false=>(
	       case moves of
		   []=>hand
		 |x::xs=>case x of
			     Draw=>(case cards of
					[]=>hand
				      |y::ys=>play(ys,xs,y::hand))
			   |Discard n=>play(cards,xs,remove_card(hand,n,IllegalMove)))
    in
	score(play(cards,moves,[]),goal)
    end
fun score_challenge(cards:card list,goal:int)=
    let val sameColor=all_same_color(cards)
	fun cur f f2 x=case x of
				[]=>0
			      |[x]=>x
			      |x::xs=>f(x,f2(xs))
	fun min x=cur Int.min min x
	fun max x=cur Int.max max x
	fun sum_cards_challenge(cards:card list,sums:int list)=
	    case cards of
		[]=>sums
	      |x::xs=>if card_value(x)=11 then sum_cards_challenge(xs,map(fn(z)=>z+1)sums@map(fn(z)=>z+11)sums)
		      else sum_cards_challenge(xs,map(fn z=>z+card_value(x))sums)
	fun initscore(x:int)=if x<goal then goal-x else 3*(x-goal)
    in
	min(map (fn x=>if sameColor then Int.div(initscore x,2) else initscore x)(sum_cards_challenge(cards,[0])))
    end
fun officiate_challenge(cards:card list,moves:move list,goal:int)=
    let	fun play(cards:card list,moves:move list,hand:card list)=
	    case score_challenge(hand,goal)>goal of
		true=>hand
	      |false=>(
	       case moves of
		   []=>hand
		 |x::xs=>case x of
			     Draw=>(case cards of
					[]=>hand
				      |y::ys=>play(ys,xs,y::hand))
			   |Discard n=>play(cards,xs,remove_card(hand,n,IllegalMove)))
    in
	score_challenge(play(cards,moves,[]),goal)
    end
fun careful_player(cards:card list,goal:int)=
    let fun decide(cards:card list,hand:card list,moves:move list)=
	    case cards of
		[]=>moves
	      |c1::c2::cs=>(case hand of
				score(hand,goal)=goal=>moves
			      |score(hand,goal)-goal>10=>decide(c2::cs,c1::hand,moves@[Draw])
			      |
			   )
