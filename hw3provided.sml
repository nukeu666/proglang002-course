(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)
fun only_capitals(st:string list)=
    List.filter (fn x=>if (Char.isUpper (String.sub(x,0)))then true else false)st
fun longest_string1(st:string list)=
    foldl (fn (x,y)=>if(String.size(x)>String.size(y))then x else y) "" st
fun longest_string2(st:string list)=
    foldl (fn (x,y)=>if(String.size(x)>=String.size(y))then x else y) "" st
fun longest_string_helper f st=foldl (fn(x,y)=>if f(String.size(x),String.size(y))=true then x else y) "" st
val longest_string3=longest_string_helper(fn(x,y)=>x>y)
val longest_string4=longest_string_helper(fn(x,y)=>x>=y)
val longest_capitalized=longest_string1 o only_capitals
val rev_string=implode o List.rev o explode

fun first_answer f l=
    let val c=map f l in
	case c of
	    []=>raise NoAnswer
	  |x::xs=>(case x of SOME v=>v
			   |NONE=>first_answer f (tl l))
    end
fun all_answers f l=
    let val c=map f l
	val cc=List.exists (fn x=>x=NONE)c 
    in
	case cc of
	    true=>NONE
	  |false=>foldl(fn(x,y)=>SOME (valOf y@valOf x))(SOME[]) c
    end

val count_wildcards=g(fn a=>1)(fn b=>1)
val count_wild_and_variable_lengths=g(fn _=>1)(fn s=>String.size s)
fun count_some_var(s,p)=g(fn _=>0)(fn a=>if a=s then 1 else 0)p

fun check_pat p=
    let fun get_strings p=case p of
			  Variable x=>[x]
			  |TupleP xx=>foldl(fn (x,y)=>(get_strings x)@y)[] xx
			  |ConstructorP (_,p)=>get_strings p
			  |_=>[]
	fun find_repeats l=case l of
			       []=>false
			     |x::xs=>List.exists(fn z=>x=z)xs
    in
	not(find_repeats (get_strings p))
    end
fun match (v,p)=case (v,p) of
		    (_,Wildcard)=>SOME[]
		  |(x,Variable y)=>SOME[(y,x)]
		  |(Constructor (a,b),ConstructorP(c,d))=>if(a=c)then match(b,d) else NONE
		  |(Unit,UnitP)=>SOME[]
		  |(Const a,ConstP b)=>if a=b then SOME[] else NONE
		  |(Tuple x,TupleP y)=> if length x=length y then all_answers match(ListPair.zip(x,y)) else NONE
		  |_=>NONE

fun first_match v p =
  SOME (first_answer (fn x => match (v, x)) p) handle NoAnswer => NONE
