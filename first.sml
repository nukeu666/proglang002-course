fun is_older(d1:(int*int*int),d2:(int*int*int))=
    if (#1 d1 < #1 d2) then true
    else if(#1 d1 > #1 d2) then false
    else if(#2 d1 < #2 d2) then true
    else if(#2 d1 > #2 d2) then false
    else if(#3 d1 < #3 d2) then true
    else if(#3 d1 > #3 d2) then false
    else false
fun number_in_month(dates:(int*int*int) list,month:int)=
    let fun match(dates:(int*int*int) list,ans:int)=
	if null dates then ans
	else if #2 (hd dates)=month then match(tl dates,ans+1)
        else match(tl dates,ans)
    in match(dates,0)
    end
fun number_in_months(dates:(int*int*int) list,months:int list)=
    if null months then 0 else number_in_months(dates,tl months)+number_in_month(dates,hd months)
fun dates_in_month(dates:(int*int*int)list,month:int)=
    if null dates 
    then [] 
    else if #2 (hd dates)=month then hd dates::dates_in_month(tl dates,month) 
    else dates_in_month(tl dates,month)
fun dates_in_months(dates:(int*int*int)list,months:int list)=
    if null months then []
    else dates_in_month(dates,hd months)@dates_in_months(dates,tl months)
fun get_nth(l:string list,n:int)=
    if n=1 then hd l
    else get_nth(tl l,n-1)
fun date_to_string(date:int*int*int)=
    let val months=["January", "February", "March", "April","May", "June", "July", "August", "September", "October", "November", "December"];
    in
	get_nth(months,#2 date)^" "^Int.toString(#3 date)^", "^Int.toString(#1 date)
    end
fun number_before_reaching_sum(n:int,l:int list)=
    let fun summer(l:int list,last:int,sum:int)=
	if hd l+sum>=n then last else summer(tl l,last+1,sum+hd l)
    in
	summer(l,0,0)
    end
fun what_month(d:int)=
    let val days=[31,28,31,30,31,30,31,31,30,31,30,31]
    in
	number_before_reaching_sum(d,days)+1
    end
fun month_range(d1:int,d2:int)=
    if d1>d2 then []
    else what_month(d1)::month_range(d1+1,d2)
fun oldest(dates:(int*int*int) list)=
    let fun older(d:(int*int*int),d2:(int*int*int))=if(is_older(d,d2)=true)then d else d2
	fun keep(datel:(int*int*int)list,ans:(int*int*int))=
	    if null datel then SOME ans
	    else keep(tl datel,older(ans,hd datel))
    in
	if null dates then NONE 
	else keep(dates,hd dates)
    end
fun number_in_months_challenge(dates:(int*int*int) list,months:int list)=
    let fun check(rem:int list,month:int)=
	    if null rem then []
	    else if hd rem=month then check(tl rem,month)
	    else hd rem::check(tl rem,month)
	fun rem_dup(all:int list,uniq:int list)=
	    if null all then uniq
	    else rem_dup(check(tl all,hd all),hd all::uniq)
    in
   	number_in_months(dates,rem_dup(months,[]))
    end
fun dates_in_months_challenge(dates:(int*int*int) list,months:int list)=
    let fun check(rem:int list,month:int)=
	    if null rem then []
	    else if hd rem=month then check(tl rem,month)
	    else hd rem::check(tl rem,month)
	fun rem_dup(all:int list,uniq:int list)=
	    if null all then uniq
	    else rem_dup(check(tl all,hd all),hd all::uniq)
    in
   	dates_in_months(dates,rem_dup(months,[]))
    end
fun reasonable_date(date:(int*int*int))=
    let val days=[0,31,28,31,30,31,30,31,31,30,31,30,31]
	fun leap_year(year:int)=
	    if year mod 400=0 then true
	    else if year mod 4=0 andalso year mod 100=0 then false
	    else if year mod 4=0 then true
	    else false
    in
	if #1 date<1 then false
	else if #2 date<1 orelse #2 date>12 then false
	else if #3 date<1 then false
	else if #2 date=2 andalso leap_year(#1 date) andalso #3 date=29 then true
	else if #3 date> (List.nth (days,#2 date)) then false
	else true
    end
